/**
 * Returns a wrapped in _function_ that can't be
 * invoked more often than once in _interval_ ms
 *
 * (wrote it myself)
 *
 * @param fn
 * @param {number} interval
 * @returns {throttleFn}
 */
function throttle (fn, interval) {
  var context = this,
      lastCall,
      timeout;

  return throttleFn;

  function throttleFn () {
    var now  = new Date(),
        diff = now - lastCall;

    if (lastCall && diff < interval) {
      timeout = timeout || setTimeout(throttleFn, diff);
      return;
    }

    if (timeout) {
      clearTimeout(timeout);
      timeout = null;
    }

    lastCall = now;

    return fn.apply(context, arguments);
  }
}
