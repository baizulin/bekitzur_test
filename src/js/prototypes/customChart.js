/**
 * Creates a custom chart
 *
 * @constructor
 *
 * @param {object} config
 * @param {HTMLElement} config.element
 * @param {number[][]} [config.data]
 * @param {number} [config.padding]
 */
function CustomChart (config) {
  var self = this;

  this.el      = config.element;
  this.data    = config.data || [];
  this.padding = config.padding || 30;

  var $el = d3.select(this.el);

  this.chart   = $el.append('g').attr('class', this.classNames.container);
  this.overlay = $el.append('rect').attr('class', this.classNames.overlay);

  this.setSize();
  this.setScales();
  this.setAxes();
  this.setLines();

  // i'm not using Function.prototype.bind here and later in the code
  // in order to save the ability to override any method if needed
  this.overlay
      .on('mousemove',
        throttle(function () {
            self.onMouseMove();
          },
          // almost imperceivable interval between callback invocations
          // frees up the message queue at little to no cost
          64)
      )
      .on('mouseenter', function () {
        self.onMouseEnter();
      })
      .on('mouseout', function () {
        self.onMouseOut();
      });
}

CustomChart.prototype.classNames = {
  container   : 'custom-chart__container',
  overlay     : 'custom-chart__overlay',
  group       : 'custom-chart__group',
  point       : 'custom-chart__point',
  hovers      : 'custom-chart__hovers',
  hoveredPoint: 'custom-chart__point--hover',
  points      : 'custom-chart__points',
  line        : 'custom-chart__line',
  axis        : 'custom-chart__axis',
  axisX       : 'custom-chart__axis--x',
  axisY       : [null, 'custom-chart__axis--y1', 'custom-chart__axis--y2']
};

CustomChart.prototype.getXValue = function (d, i) {
  return this.x(i);
};

CustomChart.prototype.getYValue = function (d) {
  return this.y(d);
};

// i wanted to make the chart fully updatable,
// but it took too much time
CustomChart.prototype.update = function (data) {
  this.data = data;
  this.setScales();
  this.updateAxes();
  this.setLines();
};

CustomChart.prototype.setSize = function () {
  var boundingRect = this.el.getBoundingClientRect();

  this.size = {
    height: boundingRect.height,
    width : boundingRect.width
  };

  this.el.setAttribute('viewBox', '0 0 ' + this.size.width + ' ' + this.size.height);

  this.size.width -= this.padding * 2;
  this.size.height -= this.padding * 2;

  var translate = 'translate(' + this.padding + ', ' + this.padding + ')';

  this.chart.attr('transform', translate);
  this.overlay
      .attr('transform', translate)
      .attr('width', this.size.width)
      .attr('height', this.size.height);
};

/**
 * Looks through the data in search of the length of the longest
 * set of data points, absolute maximum and absolute minimum values
 *
 * @returns {{ maxLength: number, max: number, min: number }}
 */
CustomChart.prototype.getDataStats = function () {
  return this.data
             // finding the length, max and min for each array
             .map(function (values) {
               return {
                 length: values.length,
                 extent: d3.extent(values)
               }
             })
             // reducing to a single stat
             .reduce(
               function (mergedStats, lineStats) {
                 return {
                   maxLength: Math.max(mergedStats.maxLength, lineStats.length),
                   max      : Math.max(mergedStats.max, lineStats.extent[1]),
                   min      : Math.min(mergedStats.min, lineStats.extent[0])
                 }
               },
               // default stat object
               { maxLength: 0, max: 0, min: 0 }
             )
};

CustomChart.prototype.setScales = function () {
  var dataStats = this.getDataStats();

  this.x = d3.scaleLinear()
             .domain([0, dataStats.maxLength - 1])
             .range([0, this.size.width]);

  this.y = d3.scaleLinear()
             .domain([dataStats.max, dataStats.min])
             .range([0, this.size.height]);
};

CustomChart.prototype.setAxes = function () {
  this.chart
      .append('g')
      .attr('class', this.classNames.axis + ' ' + this.classNames.axisX);

  this.chart
      .append('g')
      .attr('class', this.classNames.axis + ' ' + this.classNames.axisY[1]);

  this.chart
      .append('g')
      .attr('class', this.classNames.axis + ' ' + this.classNames.axisY[2]);

  this.updateAxes();
};

CustomChart.prototype.updateAxes = function () {
  this.chart
      .select('.' + this.classNames.axisX)
      .attr('transform', 'translate(0, ' + this.size.height + ')')
      .call(d3.axisBottom(this.x));

  this.chart
      .select('.' + this.classNames.axisY[1])
      .call(d3.axisLeft(this.y));

  this.chart
      .select('.' + this.classNames.axisY[2])
      .attr('transform', 'translate(' + this.size.width + ', 0)')
      .call(d3.axisRight(this.y)
              .tickSizeInner(-this.size.width)
              // 6 is the default value for d3 tick
              .tickSizeOuter(6));
};

CustomChart.prototype.setLines = function () {
  var self = this;

  var group = this
    .chart
    .selectAll('.' + this.classNames.group)
    .data(function () {
      return self.data;
    })
    .enter()
    .append('g')
    .attr('class', this.classNames.group);

  group
    .exit()
    .remove();

  // creating a line
  group
    .append('path')
    .attr('class', this.classNames.line)
    .attr('d', d3.line()
                 .curve(d3.curveCardinal)
                 .x(function (d, i) {
                   return self.getXValue(null, i);
                 })
                 .y(function (d, i) {
                   return self.getYValue(d, i);
                 }));

  // creating points
  group
    .append('g')
    .attr('class', this.classNames.points)
    .selectAll('.' + this.classNames.point)
    .data(function (d) {
      return d.map(function (d, i) {
        return { x: i, y: d };
      });
    })
    .enter()
    .append('circle')
    .attr('class', this.classNames.point)
    .attr('r', 3)
    .attr('cx', function (d) {
      return self.getXValue(null, d.x);
    })
    .attr('cy', function (d) {
      return self.getYValue(d.y);
    })
    .exit()
    .remove();

  // creating elements for hover effects
  if (!this.hovers) {
    this.hovers = {};

    this.hovers.el = this.chart
                         .append('g')
                         .attr('class', this.classNames.hovers);

    this.hovers.line = this.hovers.el
                           .append('line')
                           .attr('class', this.classNames.line);
  }

  // at first i iterated through all the points on the chart
  // setting radius based on whether the point should be hovered
  // or not, but then i decided that it would be optimal to
  // use a different set of points
  this.hovers.points = this.hovers.el
                           .selectAll('.' + this.classNames.hoveredPoint)
                           .data(function () {
                             return self.data;
                           })
                           .enter()
                           .append('circle')
                           .attr('class', this.classNames.point + ' ' + this.classNames.hoveredPoint)
                           .attr('r', 5);

  this.hovers.points
      .exit()
      .remove();
};

CustomChart.prototype.onMouseMove = function () {
  // get mouse coordinates relative to the chart/overlay
  var x = d3.mouse(this.overlay.node());

  // get the position on the x axis
  x = this.x.invert(x[0]);

  // find the closest tick on the axis
  var i = Math.round(x);

  // if the tick has not changed, do nothing
  if (this.lastHoveredTick === i) {
    return;
  }

  this.lastHoveredTick = i;

  // get the actual value for the tick
  x = this.getXValue(null, i);

  this.hovers.line
      .attr('x1', x)
      .attr('x2', x)
      .attr('y1', this.size.height)
      .attr('y2', 0);

  // iterate through the hovered points
  // and set their respective coordinates
  this.hovers.points
      .attr('cx', x)
      .attr('cy', function (d) {
        return this.getYValue(d[i]);
      }.bind(this));
};

// show/hide hovered points using CSS
// the reason i am using JS to trigger classes
// and not the CSS :hover pseudo-class
// is because in the latter situation the hovers.points
// will appear instantly and in the meantime JS
// has not finished calculating and setting their positions
// resulting in a minor bug
CustomChart.prototype.onMouseEnter = function () {
  this.chart.node().classList.add('hover');
};

CustomChart.prototype.onMouseOut = function () {
  this.chart.node().classList.remove('hover');
};
