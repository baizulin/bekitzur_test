function DataParser () {

}

/**
 * @typedef {{ x: number, val_0: number, val_1: number, ...}} ValuesMap
 */

/**
 * Parses the supplied data
 * @param {ValuesMap[]} data
 * @returns {number[][]} - parsed data
 */
DataParser.prototype.parse = function (data) {
  var parsed = [];

  data.forEach(function (valuesMap, valueIndex) {
    this.extractValues(valuesMap)
        .forEach(function (value, lineIndex) {
          parsed[lineIndex] = parsed[lineIndex] || [];
          parsed[lineIndex][valueIndex] = value;
        });
  }.bind(this));

  return parsed;
};

/**
 * Extracts values from the map
 * @param {ValuesMap} valuesMap
 * @returns {number[]} - values
 */
DataParser.prototype.extractValues = function (valuesMap) {
  return Object.keys(valuesMap)
               .filter(function (key) {
                 return /^val_\d*$/.test(key);
               })
               .map(function (key) {
                 return {
                   index: parseInt(key.substr(4)),
                   value: valuesMap[key]
                 };
               })
               .sort(function (a, b) {
                 return a.index - b.index;
               })
               .map(function (obj) {
                 return obj.value;
               });

};
