var dataParser = new DataParser();

data = dataParser.parse(data);

var $chart      = document.getElementById('custom-chart'),
    $respToggle = document.getElementById('resp-toggle');

var chart = new CustomChart({
  element: $chart,
  data   : data,
  padding: 30
});

$respToggle.addEventListener('click', function () {
  $chart.classList.toggle('custom-chart--responsive');
});
