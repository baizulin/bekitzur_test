(function () {
  'use strict';

  module.exports = function (grunt) {
    grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),

      clean  : {
        options: {
          force: true
        },
        build  : ['./assets']
      },
      less   : {
        development: {
          options: {
            paths: ["less/"]
          },
          files  : {
            './assets/css/main.css': './src/less/main.less'
          }
        },
        production : {
          options: {
            paths   : ["less/"],
            compress: true,
            cleancss: true
          },
          files  : {
            './assets/css/main.min.css': './src/less/main.less'
          }
        }
      },
      postcss: {
        options: {
          processors: [
            require('autoprefixer')({ browsers: ['last 2 version'] })
          ]
        },
        styles : {
          src: './assets/css/main*.css'
        }
      },
      concat : {
        js: {
          options: {
            banner : '(function (d3, data, undefined) {\n   \'use strict\';\n\n',
            footer : '\n})(window.d3, window.data);',
            process: function (src) {
              return '    ' + src.replace(/\n/g, '\n    ') + '\n';
            }
          },
          files  : {
            './assets/js/main.js': [
              './src/js/utils/**/*.js',
              './src/js/prototypes/**/*.js',
              './src/js/*.js'
            ]
          }
        }
      },
      uglify : {
        js: {
          src : './assets/js/main.js',
          dest: './assets/js/main.min.js'
        }
      },
      copy   : {
        vendors: {
          src    : './vendors/*',
          dest   : './assets/js/',
          expand : true,
          flatten: true
        }
      },
      watch  : {
        less: {
          files  : ['./src/less/*.less'],
          tasks  : ['less'],
          options: {
            spawn     : false,
            livereload: true
          }
        },
        js  : {
          files  : ['./src/js/**/*.js'],
          tasks  : ['concat', 'uglify'],
          options: {
            spawn     : false,
            livereload: true
          }
        }
      }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('build', ['clean', 'less', 'postcss', 'concat', 'uglify', 'copy']);
    grunt.registerTask('develop', ['build', 'watch']);
  };
})();
